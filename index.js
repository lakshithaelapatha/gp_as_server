require('dotenv').config();
const restify = require('restify')
const Logger = require('bunyan')
const corsMiddleware = require('restify-cors-middleware')

const serviceLocator = require('./app/configs/di')
const routes = require('./app/routes/index')


const logger = new Logger.createLogger({
    name: process.env.APP_NAME,
    serializers: {
        req: Logger.stdSerializers.req
    },
    streams: [
        {
            level: 'debug',
            stream: process.stdout
        },
        {
            level: 'error',
            path: 'logs/error.log'
        }
    ]
})

const server = restify.createServer({
    name: process.env.APP_NAME,
    versions: ['1.0.0'],
    log: logger
})

const cors = corsMiddleware({
    origins: ['http://localhost:8081']
})

server.pre(restify.pre.sanitizePath());
server.pre(cors.preflight)

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser())
server.use(cors.actual)

routes.register(server, serviceLocator);

server.listen(process.env.PORT, () => console.log('%s listening at %s', server.name, server.url))
