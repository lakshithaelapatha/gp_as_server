
class CatController {

    constructor(catService) {
        this.catService = catService
    }
    async get (req, res) {
        try {
            const result = await this.catService.getCats(req.log, req.query)
            res.set('Content-Type', 'image/png')
            res.sendRaw(result)

        } catch (err) {
            req.log.error(err.message);
            res.send(err);
        }
    }

}

module.exports = CatController
