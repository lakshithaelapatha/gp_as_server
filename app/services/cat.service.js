const { join, dirname } = require('path')
const { mkdirSync, existsSync } = require('fs')
const argv = require('minimist')(process.argv.slice(2))

class CatService {

    constructor(axios, blend) {
        this.axios = axios
        this.blend = blend
    }

    async getCats (log, options) {
        const { greeting, who, width, height, color, size } = options
        const _greeting = greeting || argv.greeting || process.env.GREETING
        const _who = who || argv.who || process.env.WHO
        const _width = width || argv.width || process.env.WIDTH
        const _height = height || argv.height || process.env.HEIGHT
        const _color = color || argv.color || process.env.COLOR
        const _size = size || argv.size || process.env.size

        const params = { width: _width, height: _height, c: _color, s: _size }
        const { data: img1, status: status1 } = await this.axios.get(`/says/${_greeting}`, { params, responseType: 'arraybuffer' })
        log.info(`Received response with status: ${status1}`)
        const { data: img2, status: status2 } = await this.axios.get(`/says/${_who}`, { params, responseType: 'arraybuffer' })
        log.info(`Received response with status: ${status2}`)

        const blendedImg = await this.#blendImage(_width, true, img1, img2)

        return blendedImg
    }

    /**
     * Blend Images
     * @param {Number} width
     * @param {Boolean} write
     * @param {Array} images
     * 
     * @returns {Buffer} blendedImage
     */
    async #blendImage (width, write = false, ...imgs) {
        const blendedImg = await this.blend([...imgs.map((binary, idx) => ({ src: Buffer.from(binary, 'binary'), offsetX: width * idx })),])

        if (write) {
            const fileOut = join(process.cwd(), `/output/cat-card-${new Date().getTime()}.jpg`)
            if (!existsSync(dirname(fileOut))) mkdirSync(dirname(fileOut))
            blendedImg.write(fileOut, () => console.log("The file was saved!"))
        }

        const buffer = blendedImg.getBuffer("image/jpeg", (err, img) => {
            if (err) throw err
            return img
        })

        return buffer
    }

}

module.exports = CatService