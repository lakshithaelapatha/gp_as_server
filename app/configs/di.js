const serviceLocator = require('../lib/service_locator')

serviceLocator.register('axios', () => {
    return require('axios').create({ baseURL: process.env.API })
})

serviceLocator.register('blend', () => {
    return require('combine-image')
});

/* -------------------------------- services -------------------------------- */

serviceLocator.register('catService', (serviceLocator) => {
    const CatService = require('../services/cat.service')
    const axios = serviceLocator.get('axios')
    const blend = serviceLocator.get('blend')
    return new CatService(axios, blend)
});


/* ------------------------------- controllers ------------------------------ */

serviceLocator.register('catController', (serviceLocator) => {
    const catService = serviceLocator.get('catService')
    const CatController = require('../controllers/cat.controller')

    return new CatController(catService);
});

module.exports = serviceLocator