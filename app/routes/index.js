
module.exports.register = (server, serviceLocator) => {

    server.get(
        {
            path: '/cats',
            name: 'Get Cats',
            version: '1.0.0'
        },
        (req, res, next) => serviceLocator.get('catController').get(req, res, next)
    );

}