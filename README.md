# task1

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn dev
```

### Start for production

```
yarn start
```

### CORS configuration

add client url to cors origins array in index.js

### Endpoints

| Endpoint | Method | Params                                                                         |
| -------- | ------ | ------------------------------------------------------------------------------ |
| /cats    | GET    | greeting=String,who=String,width=Number,height=Number,color=String,size=Number |
